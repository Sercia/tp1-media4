import {jeu} from "./index.js";
import {Heros} from "./Heros.js";



export class Missile extends createjs.Sprite{


    // je comprend vraiment pas !!!!! C'est quoi le super ?
    constructor(atlas){
        super(atlas);

        this.gotoAndPlay("animations");
        this.scale = 0.35;
        this.vitesse = -10;


        this.addEventListener("added", this.positionnement.bind(this));
        this.ecouteur = this.avancer.bind(this)
        createjs.Ticker.addEventListener("tick",this.ecouteur);

    }

    positionnement(){
        this.regX = this.getTransformedBounds().width;
        this.regY = this.getTransformedBounds().height / 2;
        this.rotation = -90;

        this.x = jeu.heros.x + jeu.heros.getTransformedBounds().width / 2 - 16;
        this.y = jeu.heros.y + 75;

    }


    avancer(){
        this.y += this.vitesse;

        if(this.y < -200){
        this.detruire();

        }
    }

    detruire(){
        this.parent.removeChild(this)
        createjs.Ticker.removeEventListener("tick",this.ecouteur)
    }
}