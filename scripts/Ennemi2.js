import {GabaritEnnemi} from "./GabaritEnnemi.js";
import {ProjectileEnnemi1} from "./ProjectileEnnemi1.js";
import {chargeur} from "./Chargeur.js";
import {jeu} from "./index.js";


export class Ennemi2 extends GabaritEnnemi{
    constructor(bitmap, level){
        super(bitmap);

        this.regX = this.getTransformedBounds().width / 2;
        this.regY = this.getTransformedBounds().height / 2;

        this.level = level;
        this.cadenceTire = 2000 * this.level[1];
        this.tempsEntreTir =  (this.cadenceTire * Math.random()) + this.cadenceTire;

        this.intervaleTir(this.tempsEntreTir);
        this.addEventListener("tirEnnemi", this.tirer.bind(this))

    }
    tirer(){

        createjs.Tween
            .get(this)
            .call(this.tirerUnProjectileGauche)
            .wait(300)
            .call(this.tirerUnProjectileDroite)
            .wait(300)
            .call(this.tirerUnProjectileGauche)
    }

    tirerUnProjectileGauche(){
        this.projectile = new ProjectileEnnemi1(chargeur.getResult("projectileEnnemi1"));
        jeu.stage.addChild(this.projectile);
        createjs.Sound.play("laser");

        this.projectile.x = this.x - 110;
        this.projectile.y = this.y + 45;

    }
    tirerUnProjectileDroite(){
        this.projectile = new ProjectileEnnemi1(chargeur.getResult("projectileEnnemi1"),2);
        jeu.stage.addChild(this.projectile);
        createjs.Sound.play("laser");

        this.projectile.x = this.x;
        this.projectile.y = this.y + 45;
    }





}