import {chargeur} from "./Chargeur.js";
import {jeu} from "./index.js";

export class VieUI extends createjs.Bitmap{

    constructor(vie){
        super(vie);

        this.coeur = new createjs.Bitmap(chargeur.getResult("vie"));
        this.coeur2 = new createjs.Bitmap(chargeur.getResult("vie"));
        this.encadrer = new createjs.Bitmap(chargeur.getResult("encadrer"));
        jeu.stage.addChild(this.coeur, this.coeur2, this.encadrer);
        this.propScale = 0.20;

        this.addEventListener("added", this.positionnement.bind(this));
    }

    // ne pas regarder, ya rien à voir
    positionnement(){
        this.regX = this.getTransformedBounds().width / 2;
        this.regY = this.getTransformedBounds().height / 2;
        this.coeur.regX = this.coeur.getTransformedBounds().width / 2;
        this.coeur.regY = this.coeur.getTransformedBounds().height / 2;
        this.coeur2.regX = this.coeur2.getTransformedBounds().width / 2;
        this.coeur2.regY = this.coeur2.getTransformedBounds().height / 2;

        this.encadrer.regX = this.encadrer.getTransformedBounds().width / 2;
        this.encadrer.regY = this.encadrer.getTransformedBounds().height / 2;

        this.alpha = 0.70;
        this.coeur.alpha = this.alpha;
        this.coeur2.alpha = this.alpha;
        this.encadrer.alpha = this.alpha;

        this.scale = this.propScale;
        this.coeur.scale = this.scale;
        this.coeur2.scale = this.scale;
        this.encadrer.scale = this.scale + 0.02;

        this.x = this.getTransformedBounds().width*2.3;
        this.coeur.x = this.x - 30;
        this.coeur2.x = this.x + 30;
        this.encadrer.x = this.x;

        this.y = jeu.stage.canvas.height - this.getTransformedBounds().height*2.3;
        this.coeur.y = this.y - 40;
        this.coeur2.y = this.y + 40;
        this.encadrer.y = this.y;

    }

    detruire(){
        this.parent.removeChild(this,this.coeur, this.coeur2, this.encadrer);

        createjs.Ticker.removeEventListener("tick",this.ecouteur)
    }

}