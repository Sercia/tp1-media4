import {chargeur} from "./Chargeur.js";
import {Missile} from "./Missile.js";
import {jeu} from "./index.js";

export class Heros extends createjs.Bitmap {

    constructor(bitmap){

        super(bitmap);


        this.chargeur = jeu.chargeur;

        this.scale = 0.3;
        this.alpha = 0;

        this.armePret = true;
        this.armeCharger = true;

        this.recharger = "ArrowDown";
        this.tir = "ArrowUp";
        this.right = "ArrowRight";
        this.left = "ArrowLeft";
        this.enMouvementHorizontale = false;
        this.mouvementGauche = false;
        this.mouvementDroite = false;

        this.vitesseHeros = 20;

        this.addEventListener("added", this.apparition.bind(this));

        this.ecouteurKeydown = this.gererTouchePesee.bind(this);
        window.addEventListener("keydown", this.ecouteurKeydown);
        this.ecouteurKeyup = this.gererToucheLachee.bind(this);
        window.addEventListener("keyup", this.ecouteurKeyup);

        this.ecouteurTick = this.deplacerHorizontale.bind(this)
        this.addEventListener("tick", this.ecouteurTick);
        // window.addEventListener("keyup", this.gererToucheLachee.bind(this));

    }

    apparition(){
      const bouleFeu = new createjs.Sprite(chargeur.getResult("bouleFeu"));
        this.stage.addChild(bouleFeu);
        bouleFeu.gotoAndPlay("animations");
        bouleFeu.rotation += -90;
        bouleFeu.x = this.stage.canvas.width/2 - bouleFeu.getTransformedBounds().width /2;
        bouleFeu.y = this.stage.canvas.height + bouleFeu.getTransformedBounds().height + 50;

        createjs.Tween
            .get(bouleFeu)
            .wait(500)
            .to({y: this.stage.canvas.height/ 1.3 + bouleFeu.getTransformedBounds().height}, 1500, createjs.Ease.quadOut)

        createjs.Tween
            .get(bouleFeu)
            .wait(1800)
            .to({alpha: 0}, 500, createjs.Ease.quadIn)
            .call( () => this.stage.removeChild(bouleFeu));

        createjs.Tween
            .get(this)
            .wait(1800)
            .to({alpha: 1}, 300, createjs.Ease.quadIn);
    }

    deplacerHorizontale(){
        let deuxTouchesAppuye = false;

        if(this.enMouvementHorizontale){
            const posX = this.x;
            if (this.mouvementDroite === true && this.mouvementGauche === true){
            this.x = posX;
                deuxTouchesAppuye = true
            }
            else deuxTouchesAppuye = false;
            
             if(this.mouvementGauche === true && !deuxTouchesAppuye){
                this.x -= this.vitesseHeros;
            }
            else if (this.mouvementDroite === true && !deuxTouchesAppuye) {
                this.x += this.vitesseHeros;
            }

        }

        if(this.x <= 0 ) this.x = 0;

        if(this.x >= this.stage.canvas.width - this.getTransformedBounds().width) this.x = this.stage.canvas.width - this.getTransformedBounds().width


    }

    gererTouchePesee(e){

     if(e.key === this.tir) this.tirer();

     if(e.key === this.recharger) this.chargementArme();

        else if (e.key === this.right || e.key === this.left){

            this.enMouvementHorizontale = true;

            if(e.key === this.right && e.key === this.left){
                console.log("gauche droite !")
            }

             if(e.key === this.right){
                this.mouvementDroite = true;
            }

            else if(e.key === this.left){
                this.mouvementGauche = true;
            }
        }
    }

    gererToucheLachee(e){

        if (e.key === this.right || e.key === this.left) {
            this.enMouvementHorizontale = false;

        }

        if(e.key === this.right) this.mouvementDroite = false;
        else if (e.key === this.left) this.mouvementGauche = false;

    }

    tirer(){

        if(this.armeCharger){
            let missile = new Missile(chargeur.getResult("missile"));
            this.stage.addChild(missile);
            this.cooldownArme();
            jeu.missileUI.missileTirer();
            jeu.missileUI.cooldown();
            createjs.Sound.play('rocket');

            this.dispatchEvent("heroQuiTire")
        }
    }

    chargementArme(){
        if(this.armePret){
            this.armeCharger = true;
            jeu.missileUI.missileRecharger();
        }
        else{
            clearTimeout(this.cooldownTimeout);
            this.cooldownArme();
            jeu.missileUI.cooldown();

        }

    }

    cooldownArme(){
        this.armePret = false;
        this.armeCharger = false;
        this.cooldownTimeout = setTimeout(() => this.armePret = true,1000)
    }

    explosion(){


        let explose = new createjs.Sprite(chargeur.getResult("explosionJoueur"));
        this.stage.addChild(explose);
        explose.gotoAndPlay("exb");

        explose.x = this.x + this.getTransformedBounds().width / 2;
        explose.y = this.y + this.getTransformedBounds().height / 2;
        createjs.Sound.play('explosion');


        // explose.x = this.getBounds().width / 2;
        // explose.y = this.getBounds().height / 2;

        explose.addEventListener("animationend", () => {
            // if(this.stage === null) return;
            jeu.stage.removeChild(explose);

        });



    }


    detruire(){
        // dispatchEvent("detruireExplosion");
        if(this.parent === null) return
        this.parent.removeChild(this);
        createjs.Tween.removeTweens(this);

        window.removeEventListener("keydown", this.ecouteurKeydown);
        window.removeEventListener("keyup", this.ecouteurKeyup);
        this.removeEventListener("tick", this.ecouteurTick);



    }


























}