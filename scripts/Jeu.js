import {Decor1} from "./Decor1.js";
import {Heros} from "./Heros.js";
import {chargeur} from "./Chargeur.js";
import {MissileUI} from "./MissileUI.js";
import {Ennemi1} from "./Ennemi1.js";
import {Ennemi2} from "./Ennemi2.js";
import {Missile} from "./Missile.js";
import {GabaritEnnemi} from "./GabaritEnnemi.js";
import {ProjectileEnnemi1} from "./ProjectileEnnemi1.js";
import {VieUI} from "./VieUI.js";


export class Jeu {

  constructor() {


    // Référence au canevas dans le DOM
    this.canvas = document.querySelector("canvas");

    // Paramètres modifiable du jeu
    this.params = {
      nombreBlocs: 3,
      tailleBloc: 125,
      cadence: 60,
      manifeste: "ressources/manifest.json",
      formatPolice: "48px 'ReemKufi'"

    };
    this.currentLevel = 1;
    this.nbEnnemi = 0;
    this.pointDeVie = 3;
    this.heroVulnerable = true;

    chargeur.addEventListener("complete", this.initialiser.bind(this));
    // window.addEventListener("click",this.detruireTout.bind(this));
    window.addEventListener("keydown", this.fermerInstruction.bind(this));

    //reload la page si la taille de celle-ci change
    window.addEventListener("resize", () => location.reload())

  }

  initialiser() {

    // Ajustement du canevas à la taille de la fenêtre
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    // Préparation du StageGL
    this.stage = new createjs.StageGL(this.canvas);
    this.stage.setClearColor("#000");

    // Préparation du Ticker
    createjs.Ticker.addEventListener("tick", e => this.stage.update(e));
    createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
    createjs.Ticker.framerate = this.params.cadence;

    // Ajout d'un écouteur pour la détection de collisions
    createjs.Ticker.addEventListener("tick", this.detecter.bind(this));
    
    createjs.Sound.play("music", {loop: -1, volume:0.5});

    this.ajouterDecor();
    this.commencerJeu();

  }

  ajouterDecor(){
    this.arrierePlan = new createjs.Bitmap(chargeur.getResult("fond1a"));
    this.stage.addChild(this.arrierePlan);
    this.arrierePlan.scale = 0.80;
    this.decorIntervale = setInterval(this.ajouterMeteore.bind(this), 2000);

  }

  ajouterMeteore(){
    this.meteore = new Decor1(chargeur.getResult("fond2a"));
    this.stage.addChildAt(this.meteore, 1);
  }

  instruction(){
    this.panneauInstruction = new createjs.Bitmap(chargeur.getResult("instruction"));
    this.stage.addChild(this.panneauInstruction);

    this.panneauInstruction.alpha = 0;
    this.panneauInstruction.x = this.stage.canvas.width / 2 - this.panneauInstruction.getTransformedBounds().width/2;
    this.panneauInstruction.y = this.stage.canvas.height / 2 - this.panneauInstruction.getTransformedBounds().height/2;

    createjs.Tween
        .get(this.panneauInstruction)
        .to({alpha: 1}, 500, createjs.Ease.linear)


    this.instructionOuvert = true;
  }

  fermerInstruction(){

    if (this.instructionOuvert) {
      this.instructionOuvert = false;
      createjs.Tween
          .get(this.panneauInstruction)
          .to({alpha: 0}, 250, createjs.Ease.linear)
          .call(() => {
            this.stage.removeChild(this.panneauInstruction);
          });
      this.ajouterHeros();
      this.ajouterUI();
      this.gererLevel();
    }

    if ( this.leaderBoardOuvert){
      this.leaderBoardOuvert = false;
      this.commencerJeu();
    }
  }

  ajouterHeros(){
    this.heros = new Heros(chargeur.getResult("player"));
    this.stage.addChild(this.heros);
    this.heros.x = this.stage.canvas.width/2 - this.heros.getTransformedBounds().width /2;
    this.heros.y = this.stage.canvas.height/1.3 - 25 ;

    this.heros.addEventListener("heroQuiTire", () => this.ajusterPointage(-50))
  }

  ajouterUI(){
    // UI de missile
    this.missileUI = new MissileUI(chargeur.getResult("missileUIVert"));
    this.vieUI = new VieUI(chargeur.getResult("vie"));
    this.stage.addChildAt(this.missileUI,3);
    this.stage.addChildAt(this.vieUI,3);
    this.missileUI.alpha = 0.7;
  }
  ajouterScore(){
    this.pointage = new createjs.Text("00000", this.params.formatPolice, "white");
    this.pointage.x = this.canvas.width - this.pointage.getBounds().width - 20;
    this.pointage.y = 50;
    this.pointage.cache(0, 0, this.pointage.getBounds().width, 50);
    this.stage.addChild(this.pointage);
  }

  ajusterPointage(points){
    this.pointage.text = parseInt(this.pointage.text) + points;
    this.pointage.updateCache();
  }

  ajouterPanneau(){
    this.fenetre = document.getElementById("fenetre");
    this.fenetre.style.display = "none";
    this.joueur1 = document.getElementById("joueur1");
    this.joueur2 = document.getElementById("joueur2");
    this.bouton = document.querySelector("#fenetre button");

    this.leaderBoard = document.getElementById("leaderBoard");
    this.leaderBoard.style.display = "none";
    this.nomJoueurs = document.getElementById("nomJoueurs");
    this.pointageTexte = document.getElementById("pointage");
    this.levelAtteint = document.getElementById("levelAtteint");



  }
  
  ajouterInfoJoueur(niveauAtteint){
    this.records = JSON.parse(localStorage.getItem('records'));


    if (!this.records || parseInt(this.pointage.text) > this.records.pointage) {

      this.fenetre.style.display = "block";
      this.joueur1.focus();

      // Capture des événements 'keydown'
      let ecouteur = e => {
        e.stopPropagation();
        if (e.key === "Enter") this.bouton.click();
      };

      this.joueur1.addEventListener("keydown", ecouteur);
      this.joueur2.addEventListener("keydown", ecouteur);

      // Ajout d'un écouteur sur le clic bouton OK
      this.bouton.addEventListener("click", e => {

        if (this.joueur1.value.length < 1 || this.joueur2.value.length < 1) {
          alert("Remplisser les cases !");
          return;
        }

        this.joueur1.removeEventListener("keydown", ecouteur);
        this.joueur2.removeEventListener("keydown", ecouteur);

        this.enregistrerScore(niveauAtteint);

      });
    }
    else this.enregistrerScore(niveauAtteint);


  }
  enregistrerScore(niveauAtteint) {


    // S'il n'y a pas de records dans le stockage local, on crée un objet 'records' avec les
    // pointages actuels.
    if (!this.records) {

      this.records = {
        joueur1: this.joueur1.value,
        joueur2: this.joueur2.value,
        pointage: parseInt(this.pointage.text)
      }
    }

    if (parseInt(this.pointage.text) > this.records.pointage) {
      this.records.joueur1 = this.joueur1.value;
      this.records.joueur2 = this.joueur2.value;
      this.records.pointage = parseInt(this.pointage.text);
    }
    localStorage.setItem('records', JSON.stringify(this.records));

    this.nomJoueurs.textContent = this.records.joueur1 + " et " + this.records.joueur2;
    this.pointageTexte.textContent = this.records.pointage;
    this.levelAtteint.textContent = niveauAtteint;
    this.leaderBoard.style.display = "block";


    // this.ecouteurLeaderboard = this.enleverKeydown.bind(this);
    // window.addEventListener("keydown", this.ecouteurLeaderboard)
    this.leaderBoardOuvert = true;

  }



  generationDeLevel(){
    this.level = {
      // exemple de tableau
      // level0: [nombre d'ennemi, modificateur de vitesse de tir de chaque ennemi, nombre de point gagner pour avoir fini le lvl]
      level1: [3,1,25],
      level2: [4,1,25],
      level3: [4,0.85,50],
      level4: [5,0.90,50],
      level5: [5,0.85,100],
      level6: [5,0.80,200],
      level7: [6,0.80,300],
      level8: [6,0.70,500],
      level9: [7,0.65,650],
      level10: [7,0.6,1000],
    };
  }

  gererLevel() {
    // type d'ennemis
    const cheminEnnemi = [
      chargeur.getResult("ennemi1"),
      chargeur.getResult("ennemi4")];

    let largeurEnnemi = 191.2;
    let espaceEntreEnnemi =  ((this.stage.canvas.width - largeurEnnemi * this.level["level" + this.currentLevel][0]) / (this.level["level" + this.currentLevel][0] + 1)) ;

    // positionner les ennemis celon leurs nombre
    for (let i = 0; i < this.level["level" + this.currentLevel][0]; i++){
      let nbAleatoire = Math.floor(Math.random() * 2 + 1);
      let ennemi;
      this.nbEnnemi++;

      if(nbAleatoire === 1)  ennemi = new Ennemi1(cheminEnnemi[0], this.level["level" + this.currentLevel]);
      else if(nbAleatoire === 2)  ennemi = new Ennemi2(cheminEnnemi[1],this.level["level" + this.currentLevel]);

      this.stage.addChildAt(ennemi, this.stage.getChildIndex(this.pointage) - 1);
      ennemi.x += espaceEntreEnnemi*i + ennemi.getTransformedBounds().width * i + espaceEntreEnnemi;
      ennemi.y -= 50;



    }

  }

  detecter() {
// Filtrage de la liste d'affichage pour ne retenir que les éléments qui nous intéressent
    let projectiles = this.stage.children.filter(item => item instanceof Missile);
    let ennemies = this.stage.children.filter(item => item instanceof GabaritEnnemi);

    let lazerz = this.stage.children.filter(item => item instanceof ProjectileEnnemi1);
    let heros = this.stage.children.filter(item => item instanceof Heros);

    projectiles.forEach(missile => {

      // Vérification d'une collision entre un missile et un ennemi
      ennemies.forEach(vaiseau => {

        let point = vaiseau.globalToLocal(missile.x, missile.y);

        if (vaiseau.hitTest(point.x, point.y)) {
          missile.detruire();
          vaiseau.detruire();
          this.ajusterPointage(100);

          // vérifier si le level est terminer
          this.nbEnnemi--;
          if(this.nbEnnemi === 0) {
            this.ajusterPointage(this.level["level" + this.currentLevel][2]);
            this.currentLevel++;
            this.gererLevel();
          }

        }
      });
    });

    lazerz.forEach(laser => {
      // Vérification d'une collision entre un laser et le hero

      let point = this.heros.globalToLocal(laser.x, laser.y);

      if (this.heros.hitTest(point.x, point.y)) {
        laser.detruire();
        this.heros.explosion();

        //vérifier si le hero est mort
        this.enleverPointDeVie();

      }
    });


  }

  enleverPointDeVie(){
    this.tempsInvulnerabilite = 1500;

    if (this.heroVulnerable){
      this.pointDeVie--;
      if(this.pointDeVie === 2) this.vieUI.coeur2.visible = false;
      if(this.pointDeVie === 1) this.vieUI.coeur.visible = false;

      if(this.pointDeVie === 0) this.detruireTout();
      else{
        this.heroVulnerable = false;
        setTimeout(() => {this.heroVulnerable = true;}, this.tempsInvulnerabilite);

        this.animationInvulnerabilite();
      }
    }


  }

  animationInvulnerabilite(){
    createjs.Tween
        .get(this.heros, {loop: 3})
        .to({alpha: 0.5}, 0, createjs.Ease.quadOut)
        .wait(this.tempsInvulnerabilite / 6)
        .to({alpha: 1}, 0, createjs.Ease.quadOut)
        .wait(this.tempsInvulnerabilite / 6)



  }

  detruireTout(){
    //détruire tout les ennemie
    let ennemies = this.stage.children.filter(item => item instanceof GabaritEnnemi);
    ennemies.forEach( ennemi => {
      ennemi.detruire()
    });

    //enlever tout les projectile
    let lazers = this.stage.children.filter(item => item instanceof ProjectileEnnemi1);
    lazers.forEach( lazer => {
      lazer.detruire()
    });

    //enlever projectile du hero
    let projectiles = this.stage.children.filter(item => item instanceof Missile);
    projectiles.forEach( projectile => {
      projectile.detruire()
    });

    //reset les lvl à zéro
    let niveauAtteint = this.currentLevel;
    this.currentLevel = 1;
    this.nbEnnemi = 0;
    this.pointDeVie = 3;

    //enlever hero
    this.heros.detruire();

    //enlever UI
    this.missileUI.detruire();
    this.vieUI.detruire();

    //réinistiliser point
    this.stage.removeChild(this.pointage);

    this.ajouterInfoJoueur(niveauAtteint);
  }


  commencerJeu(){
    window.removeEventListener("keydown", this.ecouteurLeaderboard);
    this.instruction();
    this.generationDeLevel();
    this.ajouterScore();
    this.ajouterPanneau();
  }













}
