import {chargeur} from "./Chargeur.js";
import {jeu} from "./index.js";

export class GabaritEnnemi extends createjs.Bitmap{

    constructor(ennemi){
        super(ennemi);

        this.scale = 0.4;
        this.rotation = 180;

        this.x = 150;
        // this.y = 100;

        this.regX = this.getTransformedBounds().width / 2;
        this.regY = this.getTransformedBounds().height / 2;

        setTimeout(this.deplacerEnnemi.bind(this), 500)

    }

    deplacerEnnemi(){

        createjs.Tween
            .get(this)
            .to({y: this.getTransformedBounds().height/ 2 + 75}, 500, createjs.Ease.quadOut)

    }

    intervaleTir(temps){
        // let tempsEntreTir = temps;

        this.intervaleTir = setInterval(this.tirer.bind(this),temps)

    }

    tirer(){
        this.dispatchEvent("tirEnnemi")

    }

    detruire(){
        this.parent.removeChild(this);
        let explose = new createjs.Sprite(chargeur.getResult("explosionJoueur"));
        //console.log(5);
        jeu.stage.addChild(explose);
        // this.stage.addChild(explose);
        explose.regX = this.getTransformedBounds().width / 2;
        explose.regY = this.getTransformedBounds().height / 2;
        explose.gotoAndPlay("exb");
        explose.x = this.x;
        explose.y += this.y;
        clearInterval(this.intervaleTir);
        createjs.Tween.removeTweens(this);
        createjs.Sound.play("explosion");

        explose.addEventListener("animationend", () => {
            jeu.stage.removeChild(explose);
        });
    }
}