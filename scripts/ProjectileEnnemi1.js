import {jeu} from "./index.js";

export class ProjectileEnnemi1 extends createjs.Bitmap{
    constructor(bitmap,typeEnnemi){
        super(bitmap);

        this.regX = this.getTransformedBounds().width;
        this.regY = this.getTransformedBounds().height /2;

        this.rotation = 90;
        this.vitesse = 9;
        if (typeEnnemi === 1) this.imprecision = Math.random() * 1.6;
        else this.imprecision = Math.random() * 1.1;

        this.ecouteur =  this.avancer.bind(this);
        createjs.Ticker.addEventListener("tick", this.ecouteur);

        this.nbAleatoire = Math.random()

    }
    avancer(){
        this.y += this.vitesse;
        if(this.nbAleatoire < 0.5) this.x += this.imprecision;
        else this.x -= this.imprecision;

        if(this.y > jeu.stage.canvas.height + 100) this.detruire();
    }

    detruire(){
        this.parent.removeChild(this)
        createjs.Ticker.removeEventListener("tick",this.ecouteur)
    }



}