import {GabaritEnnemi} from "./GabaritEnnemi.js";
import {ProjectileEnnemi1} from "./ProjectileEnnemi1.js";
import {chargeur} from "./Chargeur.js";
import {jeu} from "./index.js";


export class Ennemi1 extends GabaritEnnemi{
    constructor(bitmap, level){
        super(bitmap);

        this.regX = this.getTransformedBounds().width / 2;
        this.regY = this.getTransformedBounds().height / 2;

        this.level = level;
        this.cadenceTire = 3000 * this.level[1];

        this.tempsEntreTir =  (this.cadenceTire * Math.random()) + this.cadenceTire;

        this.intervaleTir(this.tempsEntreTir);
        this.addEventListener("tirEnnemi", this.tirer.bind(this))
    }



    tirer(){

        // super.tirer();

        createjs.Tween
            .get(this)
            .call(this.tirerUnProjectile)
            .wait(300)
            .call(this.tirerUnProjectile)
            .wait(300)
            .call(this.tirerUnProjectile)
            .wait(300)
            .call(this.tirerUnProjectile)
            .wait(300)
            .call(this.tirerUnProjectile)

    }
    tirerUnProjectile(){
        this.projectile = new ProjectileEnnemi1(chargeur.getResult("projectileEnnemi1"),1);
        jeu.stage.addChild(this.projectile);
        createjs.Sound.play("laser");

        this.projectile.x = this.x - 55;
        this.projectile.y = this.y + 45;

    }





}