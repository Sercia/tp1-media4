

export class Decor1 extends createjs.Bitmap {

    constructor(bitmap) {
        super(bitmap);

        // jeu.stage.addChild(this);
        // console.log(this.stage.canvas);

        this.valeurY = null;

        this.addEventListener("added", this.jouerFondUn.bind(this));
        // this.jouerFondUn();

        this.vitesseMeteoreX = Math.random() * 8;
        this.randomPositifNegatif = Math.random();
        if (this.randomPositifNegatif < 0.50) this.vitesseMeteoreY = Math.random() * 8;
        else this.vitesseMeteoreY = Math.random() * -8;

        let detruireMeteore = setTimeout(this.detruireLeMeteore.bind(this), 10000);

        this.alpha = 0.9;



    }


    jouerFondUn() {



        // randomize le scale de la météore
        let scaling = Math.random() - 0.10;
        if (scaling < 0.4) scaling = 0.4;
        this.scale = scaling;

        this.regX = this.getTransformedBounds().width / 2;
        this.regY = this.getTransformedBounds().height / 2;


        // Définir une position aléatoire pour le météore
        const nombrePositionMeteore = Math.random();

        if (nombrePositionMeteore < 0.50) {
            // météore à gauche
            this.valeurY = Math.random() * this.stage.canvas.height;
            this.x = -200 - this.getTransformedBounds().width / 2;
            this.y = this.valeurY;
            this.addEventListener("tick", this.meteoritesGauche.bind(this));

        } else {
            // météore à droite
            this.valeurY = Math.random() * this.stage.canvas.height;
            this.x = this.stage.canvas.width + this.getTransformedBounds().width / 2;
            this.y = this.valeurY;

            this.addEventListener("tick", this.meteoritesDroite.bind(this));


        }

    }


    meteoritesDroite() {

        this.x -= this.vitesseMeteoreX;
        this.y += this.vitesseMeteoreY;

        if (this.randomPositifNegatif < 0.50) this.rotationer = 1;
        else this.rotationer = -1;

        this.rotation += this.rotationer;

    }

    meteoritesGauche() {

        this.x += this.vitesseMeteoreX;
        this.y += this.vitesseMeteoreY;

        if (this.randomPositifNegatif < 0.50) this.rotationer = 1;
        else this.rotationer = -1;

        this.rotation += this.rotationer;
    }


    detruireLeMeteore(){
    createjs.Tween
        .get(this)
        .to({alpha: 0}, 500, createjs.Ease.linear)
        .call( () => this.stage.removeChild(this));



    }










}


