import {chargeur} from "./Chargeur.js";
import {jeu} from "./index.js";

export class MissileUI extends createjs.Bitmap{

    constructor(missileVert){
        super(missileVert);

        this.missileRouge = new createjs.Bitmap(chargeur.getResult("missileUIRouge"));
        this.boiteRouge = new createjs.Bitmap(chargeur.getResult("boiteRouge"));
        this.encadrer = new createjs.Bitmap(chargeur.getResult("encadrer"));
        jeu.stage.addChild(this.missileRouge, this.boiteRouge, this.encadrer);
        this.propScale = 0.20;

        this.addEventListener("added", this.positionnement.bind(this));
    }

    // ne pas regarder, ya rien à voir
    positionnement(){
        //changer le point de transformation
        this.regX = this.getTransformedBounds().width / 2;
        this.regY = this.getTransformedBounds().height / 2;
        this.missileRouge.regX = this.missileRouge.getTransformedBounds().width / 2;
        this.missileRouge.regY = this.missileRouge.getTransformedBounds().height / 2;
        this.boiteRouge.regX = this.boiteRouge.getTransformedBounds().width / 2;
        // this.boiteRouge.regY = this.boiteRouge.getTransformedBounds().height / 2;
        this.encadrer.regX = this.encadrer.getTransformedBounds().width / 2;
        this.encadrer.regY = this.encadrer.getTransformedBounds().height / 2;

        //changer l'alpha
        this.alpha = 0.70;
        this.missileRouge.alpha = this.alpha;
        this.boiteRouge.alpha = this.alpha;
        this.encadrer.alpha = this.alpha;


        //changer le scale
        this.scale = this.propScale;
        this.missileRouge.scale = this.scale;
        this.boiteRouge.scale = this.scale + 0.02;
        this.encadrer.scale = this.scale + 0.02;

        // positionner sur les x
        this.x = jeu.stage.canvas.width  - this.getTransformedBounds().width - this.getTransformedBounds().width /4;
        this.missileRouge.x = this.x;
        this.boiteRouge.x = this.x;
        this.encadrer.x = this.x;

        this.y = jeu.stage.canvas.height -  this.getTransformedBounds().height;
        this.missileRouge.y = this.y;
        this.boiteRouge.y = this.y - this.boiteRouge.getTransformedBounds().height /2;
        this.encadrer.y = this.y;

        this.missileRouge.visible = false;
        this.boiteRouge.alpha = 0.80;
        this.boiteRouge.scaleY = 0;
    }

    missileTirer(){
        this.missileRouge.visible = true;
    }

    missileRecharger(){
        this.missileRouge.visible =false;
    }

    cooldown() {
        this.boiteRouge.scaleY = this.propScale + 0.02;


        createjs.Tween
            .get(this.boiteRouge)
            .to({scaleY: 0}, 1000)
            .call(() => this.boiteRouge.scaleY = 0)
    }

    detruire(){
        this.parent.removeChild(this,this.missileRouge, this.boiteRouge, this.encadrer);
        createjs.Tween.removeTweens(this.boiteRouge);

        createjs.Ticker.removeEventListener("tick",this.ecouteur)
    }





}